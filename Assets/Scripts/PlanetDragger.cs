﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetDragger : MonoBehaviour {

    GameObject selectedObject;
    GameObject closestObject;
    Vector3 mouseStartDrag;

	// Use this for initialization
	void Start () {
		
        GetComponent<LineRenderer>().enabled = false;
    }

    // Update is called once per frame
    void Update () {

        Vector3 mouseLocation = Input.mousePosition;
        mouseLocation = Camera.main.ScreenToWorldPoint(mouseLocation);
        mouseLocation.z = 0f;

        float closestDistance = 100000f;

        if (selectedObject)
        {
            GetComponent<LineRenderer>().SetPositions(new Vector3[] {
                new Vector3(mouseLocation.x, mouseLocation.y, 30),
                new Vector3(mouseStartDrag.x, mouseStartDrag.y, 30)});
        }
        GameObject tempClosest = null;
        foreach (GameObject gameObject in GameObject.FindGameObjectsWithTag("Planet"))
        {
            float distanceToObject = (gameObject.transform.position - mouseLocation).sqrMagnitude;
            if (distanceToObject < closestDistance && gameObject.GetComponent<PlanetBehavior>() != null && gameObject.GetComponent<PlanetBehavior>().CanBeInteractedWith())
            {
                closestDistance = distanceToObject;
                tempClosest = gameObject;
            }
        }

        if (closestDistance > 150f)
        {

            if (closestObject)
            {
                closestObject.GetComponent<PlanetBehavior>().StopGlowing();
                closestObject = null;
            }

            return;
        }

        if (closestObject && tempClosest != closestObject)
        {
            closestObject.GetComponent<PlanetBehavior>().StopGlowing();
        }

        if (tempClosest)
        {
            closestObject = tempClosest;

            closestObject.GetComponent<PlanetBehavior>().StartGlowing();
        }

    }

    private void OnMouseDown()
    {
        Vector3 mouseLocation = Input.mousePosition;
        mouseLocation.z = 0f;
        mouseLocation = Camera.main.ScreenToWorldPoint(mouseLocation);


        selectedObject = closestObject;
        mouseStartDrag = mouseLocation;

        if (selectedObject)
        {
            GetComponent<LineRenderer>().enabled = true;
            selectedObject.GetComponent<PlanetBehavior>().StartGlowing();
        }

    }

    private void OnMouseUp()
    {
        GetComponent<LineRenderer>().enabled = false;

        if (!selectedObject)
        {
            return;
        }

        Vector3 mouseLocation = Input.mousePosition;
        mouseLocation.z = 0f;
        mouseLocation = Camera.main.ScreenToWorldPoint(mouseLocation);

        if ((mouseLocation - mouseStartDrag).sqrMagnitude < 1)
        {
            return;
        }

        selectedObject.GetComponent<Rigidbody>().velocity = (mouseLocation - mouseStartDrag) * 1.5f;

        GameState state = FindObjectOfType<GameState>();

        if (state.CurrentGameState == EGameState.Pregame)
        {
            state.CurrentGameState = EGameState.Ingame;

            ScoreCounter score = FindObjectOfType<ScoreCounter>();
            score.ResetScore();
            score = FindObjectOfType<ScoreCounter>();

            PlanetSpawner spawner = FindObjectOfType<PlanetSpawner>();
            spawner.StartSpawning();
            GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Play();

        }
    }
}
