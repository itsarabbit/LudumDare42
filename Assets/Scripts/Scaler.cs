﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Scaler : MonoBehaviour {

    float StartScale = 1f;
    float GoalScale = 1f;
    float CurrentProgress = 0f;

	// Use this for initialization
	void Start () {
		
	}

    public bool IsScalingUpwards()
    {
        return StartScale < GoalScale;
    }
	
	// Update is called once per frame
	void Update () {
        CurrentProgress += Time.deltaTime;

        if (CurrentProgress > 1f)
            CurrentProgress = 1f;

        float scale = GetCurrentScale();
        transform.localScale = new Vector3(scale, scale, scale);
    }

    public bool IsScaling()
    {
        return CurrentProgress < 1f;
    }

    public float GetCurrentScale()
    {
        return Mathf.SmoothStep(StartScale, GoalScale, CurrentProgress);
    }

    public void SetScale(float scale)
    {
        StartScale = transform.localScale.x;
        GoalScale = scale;
        CurrentProgress = 0f;
    }
}
