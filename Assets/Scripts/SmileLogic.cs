﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class SmileLogic : MonoBehaviour {

    public Sprite Smile;
    public Sprite Succ;
    public NomPlanets BlackHole;
    SpriteRenderer Renderer;

    int ShakeIndex = 0;

	// Use this for initialization
	void Start () {
        Renderer = GetComponent<SpriteRenderer>();

    }
	
	// Update is called once per frame
	void FixedUpdate ()
    {
        ShakeIndex++;

        if (BlackHole.GetComponent<Scaler>().IsScaling() && BlackHole.GetComponent<Scaler>().IsScalingUpwards())
        {
            if (ShakeIndex >= 2)
            {
                float range = 0.3f;
                transform.position = new Vector3(Random.Range(-range, range), Random.Range(-range, range), -150);
                ShakeIndex = 0;
            }
            Renderer.sprite = Succ;
        }
        else
        {
            transform.position = new Vector3(0, 0, -150);
            Renderer.sprite = Smile;
        }
	}
}
