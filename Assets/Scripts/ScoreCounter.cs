﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class ScoreCounter : MonoBehaviour {

    int Score = 0;
    GameState GameState;
	// Use this for initialization
	void Start () {
        GameState = FindObjectOfType<GameState>();
	}
	
	// Update is called once per frame
	void Update () {
        GetComponent<TextMesh>().text = Score.ToString();
    }

    public void AddScore(int score)
    {
        if (GameState.CurrentGameState == EGameState.Ingame)
        {
            Score += score;
        }
    }

    public void ResetScore()
    {
        Score = 0;
    }
}
