﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetSpawner : MonoBehaviour {

    public GameObject PlanetPrefab;
    public GameObject BlackHole;
    int planetsSpawned = 0;

    // Use this for initialization
    void Start () {
    }
	
    public void StartSpawning()
    {
        planetsSpawned = 0;
        StartCoroutine(SpawnPlanetCoroutine());
    }

    public void StopSpawning()
    {
        StopAllCoroutines();
    }

    // Update is called once per frame
    void Update () {
	}

    public void SpawnPlanet(bool instantiateImmediately)
    {
        planetsSpawned++;
        float angle = Random.Range(0, 360f);
        float distance = Random.Range(10, 30f);

        var x = Mathf.Sin(angle * Mathf.Deg2Rad) * distance;
        var y = Mathf.Cos(angle * Mathf.Deg2Rad) * distance;

        GameObject planet = Instantiate(PlanetPrefab, new Vector3(x, y), Quaternion.identity);

        planet.GetComponent<PlanetBehavior>().BlackHole = BlackHole;

        if (Random.Range(0, 2) == 1)
        {
            planet.GetComponent<Rigidbody>().velocity = new Vector3(-y, x, 0) * 5f * 0.1f;
        }
        else
        {
            planet.GetComponent<Rigidbody>().velocity = new Vector3(-y, x, 0) * -5f * 0.1f;
        }
        planet.GetComponent<Rigidbody>().velocity += Random.insideUnitSphere * 1f * 0.1f;
        planet.GetComponent<Rigidbody>().velocity = new Vector3(planet.GetComponent<Rigidbody>().velocity.x, planet.GetComponent<Rigidbody>().velocity.y, 0);
        if (!instantiateImmediately)
        {
            planet.GetComponent<PlanetBehavior>().OnSpawned();
        }
    }

    IEnumerator SpawnPlanetCoroutine()
    {
        loop:

        yield return new WaitForSeconds(10 + planetsSpawned);
       // yield return new WaitForSeconds(1);

       SpawnPlanet(false);


        goto loop;
    }
}
