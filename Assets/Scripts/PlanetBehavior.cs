﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PlanetBehavior : MonoBehaviour {

    public GameObject BlackHole;

    private Texture2D noiseTex;
    private Color[] pix;
    private Renderer rend;

    private Color BaseColor;
    private Color SecondaryColor;
    bool IsEnabled = true;
    bool IsDead = false;

    public ParticleSystem CollisionParticles;
    public ParticleSystem SelectionParticles;

    ParticleSystem SelectionParticlesObject;

    float StartScale = 3f;

    Vector3 StartPosition;


    // Use this for initialization
    void Start () {
        GenerateNoise();

        GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * Random.Range(0f, 10f);

        GetComponent<TrailRenderer>().startColor = BaseColor;
        GetComponent<TrailRenderer>().endColor = SecondaryColor;

        StartScale = Random.Range(2.5f, 3.5f);

        GetComponent<Scaler>().SetScale(StartScale);

        GetComponents<AudioSource>()[1].pitch = Random.Range(0.8f, 1.2f) * ( 1 + (3.5f - StartScale));

        SelectionParticlesObject = Instantiate(SelectionParticles, transform);


        var emission = SelectionParticlesObject.emission;
        emission.enabled = false;
    }

    public void OnSpawned()
    {
        GetComponent<Collider>().enabled = false;
        GetComponent<TrailRenderer>().enabled = false;
        GetComponent<MeshRenderer>().enabled = false;
        IsEnabled = false;
        StartPosition = transform.position;
        
    }

    public void Kill(float time = 20)
    {
        IsDead = true;

        GetComponent<Collider>().enabled = false;
        GetComponent<Scaler>().SetScale(0f);
        GetComponent<ParticleSystem>().enableEmission = false;

        tag = "Untagged";
        Destroy(this, time);
    }

    void EnterGame()
    {
        GetComponent<Collider>().enabled = true;
        GetComponent<TrailRenderer>().enabled = true;
        GetComponent<MeshRenderer>().enabled = true;
        IsEnabled = true;

        transform.localScale = new Vector3(0f, 0f, 0f);
        GetComponent<Scaler>().SetScale(StartScale);
    }

    public bool CanBeInteractedWith()
    {
        return IsEnabled && !IsDead;
    }

    void GenerateNoise()
    {
        float Hue = Random.Range(0f, 1f);
        float Saturation = Random.Range(0.5f, 1f);
        float HueBackground = Random.Range(0f, 1f);
        float Brightness = Random.Range(0.75f, 1f);

        BaseColor = Color.HSVToRGB(Hue, Saturation, Brightness);
        SecondaryColor = Color.HSVToRGB(HueBackground, 1f, 1f);

        rend = GetComponent<Renderer>();

        // Set up the texture and a Color array to hold pixels during processing.
        noiseTex = new Texture2D(128, 128);
        pix = new Color[noiseTex.width * noiseTex.height];
        //rend.material.mainTexture = noiseTex;
        rend.material.SetTexture("_EmissionMap", noiseTex);

        // For each pixel in the texture...
        float y = 0.0F;

        Vector2 offset = Random.insideUnitCircle * 20000f;

        float scale = Random.Range(5f, 20);

        while (y < noiseTex.height)
        {
            float x = 0.0F;
            while (x < noiseTex.width)
            {
                float xCoord = offset.x + x / noiseTex.width * scale;
                float yCoord = offset.y + y / noiseTex.height * scale;
                float sample = Mathf.PerlinNoise(xCoord, yCoord);
                Color color = Color.HSVToRGB(Hue, Saturation + sample, Brightness);
                pix[(int)y * noiseTex.width + (int)x] = Color.Lerp(color, Color.HSVToRGB(HueBackground, 1.0f, 1.0f), sample) * (1f + Mathf.Pow(sample, 3f) * 3f);
                x++;
            }
            y++;
        }

        // Copy the pixel data to the texture and load it into the GPU.
        noiseTex.SetPixels(pix);
        noiseTex.Apply();
    }

    public void StartGlowing()
    {
        var emission = SelectionParticlesObject.emission;
        emission.enabled = true;
    }

    public void StopGlowing()
    {
        var emission = SelectionParticlesObject.emission;
        emission.enabled = false;
    }
	
	// Update is called once per frame
	void Update () {
        Rigidbody body = GetComponent<Rigidbody>();

        if (!CanBeInteractedWith() && !IsDead)
        {
            float currentDistance = (StartPosition - transform.position).magnitude;
            if (currentDistance < 5f)
            {
                if ((StartPosition - (transform.position + body.velocity * Time.deltaTime)).magnitude < currentDistance)
                {
                    EnterGame();
                }
            }
        }

        GameState gameState = FindObjectOfType<GameState>();

        if (gameState.CurrentGameState != EGameState.Ingame || IsDead)
        {
            body.velocity = new Vector3(0, 0, 0);
            return;
        }

        Vector2 DirectionToBlackHole = BlackHole.transform.position - transform.position;
        float lengthToBlackHole = DirectionToBlackHole.magnitude;


        Vector2 velocity = DirectionToBlackHole * Time.deltaTime + (DirectionToBlackHole.normalized * Time.deltaTime) / (1f / DirectionToBlackHole.magnitude) * 5 * BlackHole.GetComponent<NomPlanets>().GravityPull;

        body.velocity += new Vector3(velocity.x, velocity.y) * 0.1f;
        //transform.position += new Vector3(Velocity.x, Velocity.y) * Time.deltaTime * 0.1f;

	}

    private void FixedUpdate()
    {
        if (!CanBeInteractedWith())
            return;

        ScoreCounter counter = FindObjectOfType<ScoreCounter>();
        Vector2 DirectionToBlackHole = BlackHole.transform.position - transform.position;
        float lengthToBlackHole = DirectionToBlackHole.magnitude;

        if (lengthToBlackHole < 10)
        {
            counter.AddScore(1);
        }
        
        if (lengthToBlackHole < 25)
        {
            counter.AddScore(1);
        }

        Rigidbody body = GetComponent<Rigidbody>();
        if (body.velocity.magnitude > 10)
        {
            counter.AddScore(1);
        }

        if (body.velocity.magnitude > 25)
        {
            counter.AddScore(1);
        }
    }

    private void OnCollisionEnter(Collision collision)
    {
        ScoreCounter counter = FindObjectOfType<ScoreCounter>();
        if (collision.gameObject.CompareTag("Planet"))
        {
            counter.AddScore(200);
            counter.AddScore((int)collision.impulse.magnitude * 10);

            GetComponent<AudioSource>().volume = collision.impulse.magnitude / 100;
            GetComponent<AudioSource>().Play();


            ParticleSystem particles = Instantiate(CollisionParticles, collision.contacts[0].point, Quaternion.LookRotation(collision.contacts[0].normal, new Vector3(0, 0, 1)));
            var emission = particles.emission;
            var settings = particles.main;
            settings.startColor = BaseColor * 4;
            emission.SetBurst(0, new ParticleSystem.Burst(0f, collision.impulse.magnitude * 3));

            //particles.Emit((int)(collision.impulse.magnitude * 100));

        }
    }
}
