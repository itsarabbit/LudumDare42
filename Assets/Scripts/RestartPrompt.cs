﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class RestartPrompt : MonoBehaviour {

	// Use this for initialization
	void Start () {
		
	}
	
	// Update is called once per frame
	void Update () {

        switch (FindObjectOfType<GameState>().CurrentGameState)
        {
            case EGameState.Pregame:
                GetComponent<TextMesh>().color = new Color(1, 1, 1, 255);
                GetComponent<TextMesh>().text = "Drag the planet to get going!\nMade by Nicolas Gustafsson for Ludum Dare 42";

                break;
            case EGameState.Ingame:
                GetComponent<TextMesh>().color = new Color(1, 1, 1, 0);

                break;
            case EGameState.GameOver:
                GetComponent<TextMesh>().color = new Color(1, 1, 1, 255);
                if (Input.GetMouseButtonUp(1))
                {
                    foreach (TrailRenderer trail in FindObjectsOfType<TrailRenderer>())
                    {
                        trail.enabled = false;
                    }

                    FindObjectOfType<GameState>().StartPregame();
                }

                GetComponent<TextMesh>().text = "Right Click to restart!";

                break;
        }
    }
}
