﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public enum EGameState
{
    Pregame,
    Ingame,
    GameOver
}

public class GameState : MonoBehaviour {

    public EGameState CurrentGameState = EGameState.Ingame;

	// Use this for initialization
	void Start () {
        StartPregame();
    }

    public void StartPregame()
    {
        CurrentGameState = EGameState.Pregame;
        NomPlanets nommer = FindObjectOfType<NomPlanets>();
        nommer.Reset();
        PlanetSpawner spawner = FindObjectOfType<PlanetSpawner>();
        spawner.SpawnPlanet(true);
    }

    // Update is called once per frame
    void Update () {
		
	}
}
