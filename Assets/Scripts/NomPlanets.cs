﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class NomPlanets : MonoBehaviour {

    public float GravityPull = 1f;
    int nomedPlanets = 1;

    private Texture2D noiseTex;
    private Color[] pix;
    private Renderer rend;

    Vector3 StartScale = new Vector3(5.5f, 5.5f, 5.5f);
    Vector3 ParticlesStartScale = new Vector3(1, 1, 1);
    float currentOffset = 0;   



    // Use this for initialization
    void Start () {
        StartScale = transform.localScale;
        GenerateNoise();
        //GetComponent<Rigidbody>().angularVelocity = Random.insideUnitSphere * Random.Range(0.1f, 2f);
    }

    public void Reset()
    {
        nomedPlanets = 1;
        //transform.localScale = StartScale;
        GetComponent<Scaler>().SetScale(StartScale.x);
        //gameObject.transform.GetChild(0).localScale = ParticlesStartScale;
        GravityPull = 1f;
    }

    // Update is called once per frame
    void Update () {
        currentOffset += Time.deltaTime * 0.1f;
        rend.material.mainTextureOffset = new Vector2(currentOffset, 0f);
    }

    void GenerateNoise()
    {
        rend = GetComponent<Renderer>();

        // Set up the texture and a Color array to hold pixels during processing.
        noiseTex = new Texture2D(256, 256);
        pix = new Color[noiseTex.width * noiseTex.height];
        //rend.material.mainTexture = noiseTex;
        rend.material.mainTexture = noiseTex;

        // For each pixel in the texture...
        float y = 0.0F;

        Vector2 offset = Random.insideUnitCircle * 20000f;

        float scale = Random.Range(5f, 20);

        while (y < noiseTex.height)
        {
            float x = 0.0F;
            while (x < noiseTex.width)
            {
                float xCoord = offset.x + x / noiseTex.width * scale;
                float yCoord = offset.y + y / noiseTex.height * scale;
                float sample = Mathf.PerlinNoise(xCoord, yCoord) * 0.25f;
                pix[(int)y * noiseTex.width + (int)x] = new Color(sample, sample, sample);
                x++;
            }
            y++;
        }

        // Copy the pixel data to the texture and load it into the GPU.
        noiseTex.SetPixels(pix);
        noiseTex.Apply();
    }


    private void OnTriggerEnter(Collider other)
    {
        if (other.gameObject.CompareTag("Planet") && gameObject.transform.localScale.x < 140)
        {
            other.gameObject.GetComponent<PlanetBehavior>().Kill(10f);
            //gameObject.transform.localScale = new Vector3(gameObject.transform.localScale.x, gameObject.transform.localScale.y, gameObject.transform.localScale.z) * ((nomedPlanets * nomedPlanets) * 0.1f + 1f);
            float goalScale = gameObject.transform.localScale.x * ((nomedPlanets * nomedPlanets) * 0.1f + 1f);
            GetComponent<Scaler>().SetScale(goalScale);
           // gameObject.transform.GetChild(0).localScale = new Vector3(gameObject.transform.GetChild(0).localScale.x, gameObject.transform.GetChild(0).localScale.y, gameObject.transform.GetChild(0).localScale.z) * ((nomedPlanets * nomedPlanets) * 0.1f + 1f);
            GravityPull *= 1.2f;
            nomedPlanets++;

            other.gameObject.GetComponents<AudioSource>()[1].Play();

            if (goalScale >= 40)
            {
                GetComponent<Scaler>().SetScale(200);
                //gameObject.transform.localScale = new Vector3(200, 200, 200);
               // gameObject.transform.GetChild(0).localScale = new Vector3(0, 0, 0);

                
                FindObjectOfType<GameState>().CurrentGameState = EGameState.GameOver;
                FindObjectOfType<PlanetSpawner>().StopSpawning();

                foreach(GameObject gameObject in GameObject.FindGameObjectsWithTag("Planet"))
                {
                    gameObject.GetComponent<PlanetBehavior>().Kill(1f);
                }

                GameObject.FindGameObjectWithTag("BGM").GetComponent<AudioSource>().Stop();
            }
        }
    }
}
